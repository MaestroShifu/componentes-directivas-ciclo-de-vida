import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.css']
})
export class ClassesComponent implements OnInit {

  public alert: string;
  public propiedades: Object;

  constructor() {
    this.alert = 'alert-success';

    this.propiedades = {
      danger: false
    };
  }

  ngOnInit() {
  }

}
