import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute) {
    //toma id del padre
    this.activatedRoute.parent.params.subscribe((params: any) => {
      console.log("Ruta padre", params);
    });
   }

  ngOnInit() {
  }

}
